package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

import model.data_structures.Node;
import model.data_structures.RedBlackBST;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo<K,V> {
	private static final boolean BLACK = false;
	/**
	 * Atributos del modelo del mundo
	 */
	public RedBlackBST datos;
	private int[] puntos;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new RedBlackBST();
	}
	
	public String cargarDatos() throws Exception{
		String reporte="";
		Node n;
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(new FileReader("./data/bogota_cadastral.json"));
		   String ptype = obj.get("type").getAsString();
		   int cont=0;
		   JsonArray arr = (JsonArray) obj.get("features").getAsJsonArray();
		   puntos = new int[arr.size()];
		   for(int i =0; arr!=null && i < arr.size();i++)
		   {
		    JsonObject aux = (JsonObject) arr.get(i);
		    
		    n=new Node("", "", 1, false);
		    JsonObject pgeometry = (JsonObject)aux.get("geometry");
		    JsonArray pcoordinates= (JsonArray)pgeometry.get("coordinates");

		    
		    String scoord="";
		    for(int j=0; j<pcoordinates.size();j++)
		    	{
		    	scoord+=String.valueOf(pcoordinates.get(j));
		    	}


		    JsonObject prop= (JsonObject) aux.get("properties");
		    JsonPrimitive mov=(JsonPrimitive)prop.get("MOVEMENT_ID");
		    int smov=mov.getAsInt();

		    JsonPrimitive shapeL=(JsonPrimitive)prop.get("shape_leng");
		    String sshapeL=shapeL.getAsString();
		    
		    JsonPrimitive shapeA=(JsonPrimitive)prop.get("shape_area");
		    String sshapeA=shapeA.getAsString();
		    
		    JsonPrimitive nom=(JsonPrimitive)prop.get("scanombre");
		    String snom=nom.getAsString();
		    puntos[i] = scoord.split(",").length;
		    cont++;
		    datos.put(smov,snom+","+sshapeL+","+sshapeA+","+scoord.split(",").length);
		    
		    
		   }
		   reporte+="\nSe cargaron "+cont+" zonas.";
		   
		return reporte;
	}
	
	public String consultarZonaPorId(int id) {
		String m = (String) datos.get(id);
		String[] n = m.split(",");
		
		String ret = "Nombre de la zona: "+n[0]+"\nPerimetro: "+Double.parseDouble(n[1])*100+" Km\nArea: "+Double.parseDouble(n[2])*10000+" Km2\nNumero de puntos: "+puntos[id-1];
		return ret;
	}
	
	public String consultarZonaConIdEnRangoZonas(int movIdInferior, int movIdMaximo) {
		String ret = "";
		for(int i = movIdInferior; i <= movIdMaximo; i++) {
			String m = (String) datos.get(i);
			String[] n = m.split(",");
			ret="MOVEMENT_ID: "+(i)+"\nNombre de la zona: "+n[0]+"\nPerimetro: "+Double.parseDouble(n[1])*100+" Km\nArea: "+Double.parseDouble(n[2])*10000+" Km2\nNumero de puntos: "+puntos[i-1]+"\n";
			System.out.println(ret);
		}
		return ret;
	}
}
