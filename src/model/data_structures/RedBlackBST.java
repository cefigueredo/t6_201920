package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class RedBlackBST <K , V >  {

	
	private int size;
	private final static boolean RED = true;
	private final static boolean BLACK = false;
	public Node<K,V> root;
	boolean color;

	private Node<K,V> left;
	private Node<K,V> right;
	private ArrayList<Node> inorden;

	public RedBlackBST(){
		root = null;
		size = 0;
		color = BLACK;
		inorden=new ArrayList<>();
	}
	/*******************************************************************************/
	public class Node<K,V> {
		
		private final static boolean RED = true;
		private final static boolean BLACK = false;
		private K key;
		private V value;
		int N;
		boolean color;
		private Node<K,V> left;
		private Node<K,V> right;
		
		public Node(K key, V value, int N, boolean color){
			this.setKey(key);
			this.setValue(value);
			this.N = N;
			this.color = color;
		}
		
		private boolean isRed(Node x){
			if(x == null)
				return false;
			return x.color == RED;
		}
		
		private Node<K,V> getLeft(){
			return left;
		}

		public K getKey() {
			return key;
		}

		public void setKey(K key) {
			this.key = key;
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}
	}
	/*******************************************************************************/
	public int size(Node x){
		return size;
	}
	
	public boolean isEmpty(){
		return (size == 0 )?true:false;
	}
	
	public V get(K key){
		Node x=root;
		while(x!=null)
		{
			int c=((int)key-(int)x.getKey());
			if(c<0) x=x.left;
			else if(c>0)x=x.right;
			else return (V) x.getValue();
		}
		return null;
	}
	
	public int getHeight(K key) {
		int rta=0;
		if(get(key)!=null)
		{
			Node x=root;
			while(x.getKey()!=key)
			{
				int c=((int)key-(int)x.getKey());
				if(c<0) x=x.left;
				else if(c>0)x=x.right;
				rta++;
			}
		}
		else rta=-1;
		return rta;
	}
	
	public boolean contains(K key) {
		return get(key)!=null;
	}
	
	public void put(K key, V val) throws Exception {
		Node x=root;
		if(key==null || val==null) throw new Exception();
		else if(contains(key)) 
		{
			while(x.getKey()!=key)
			{
				int c=((int)key-(int)x.getKey());
				if(c<0) x=x.left;
				else if(c>0)x=x.right;
			}
			x.setValue(val);
		}
		else {
			root = put(root, key, val);
			root.color = BLACK;
		}
	}
	private Node put(Node h, K key, V val) {
		if (h == null)
		return new Node(key, val, 1, RED);
		int cmp=((int)key-(int)h.getKey());
		if (cmp < 0)
		h.left = put(h.left, key, val);
		else if (cmp > 0)
		h.right = put(h.right, key, val);
		else
		h.setValue(val);
		// fix-up any right-leaning links
		if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right)) flipColors(h);
		h.N = size(h.left) + size(h.right) + 1;
		size++;
		return h;
		}
	private boolean isRed(Node x) {
		// TODO Auto-generated method stub
		if(x==null)return false;
		return x.color;
	}

	private Node rotateLeft(Node h)
	{
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left) + size(h.right);
		return x;
		}
	private Node rotateRight(Node h)
	{
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;	
		x.N = h.N;
		h.N = 1 + size(h.left) + size(h.right);
		return x;
		}
	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
		}
	public int height(){
		Node x=root;
		int max=0;
		inorden(root);
		for(Node actual: inorden)
		{
			if(actual.N>max) max=actual.N;
		}
		return max;
	}
	public void inorden(Node x)
	{
		
		Node aux=x;
		if(aux.left!=null)
		{
			inorden(aux.left);
		}
		inorden.add(aux);
		if(aux.right!=null)
		{
			inorden(aux.right);
		}
		
	}
	
	public K min(){
		if(size==0) return null;
		else
		{
			Node x=root;
			while(x.left!=null) x=x.left;
			return (K) x.getKey();
		}
	}
	
	@SuppressWarnings("unchecked")
	public K max(){
		if(size==0) return null;
		else
		{
			Node x=root;
			while(x.right!=null) x=x.right;
			return (K) x.getKey();
		}
	}
	
	public boolean check(){
		Node x=root;
		boolean rta=true;
		if(((int) x.getKey()-(int) x.left.getKey())<=0 ||
				(int) x.getKey()-(int) x.left.getKey()>=0 ||
				isRed(x.right))
		{
			return false;
		}
		else if(isRed(x.left)&& isRed(x.left.left))
		{
			return false;
			
		}
		else
		{
			check(x.left);
			check(x.right);
			return rta;
		}

	
	}
	private boolean check(Node x)
	{
		boolean rta=true;
		if(((int) x.getKey()-(int) x.left.getKey())<=0 ||
				(int) x.getKey()-(int) x.left.getKey()>=0 ||
				isRed(x.right))
		{
			return false;
		}
		else if(isRed(x.left)&& isRed(x.left.left))
		{
			return false;
			
		}
		else
		{
			check(x.left);
			check(x.right);
			return rta;
		}
	}
	public Iterator <K> keys() {
		ArrayList<Node> aux =inorden;
		Iterator<K> rta=new Iterator<K>() {
			private Node next=aux.get(0);
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return(posInorden((K) next.getKey())<aux.size());
			}

			@Override
			public K next() {
				// TODO Auto-generated method stub
				next=aux.get(posInorden((K) next.getKey())+1);
				return (K) next.getKey();
			}
			
		};
		return rta;

	}
	private int posInorden(K key)
	{
		for(int i=0;i<inorden.size();i++)
		{
			if(((String) inorden.get(i).getKey()).compareTo((String) key)==0) return i;
		}
		return 0;
	}
	
	public Iterator<V> valuesInRange(K init, K end){
		ArrayList<Node> aux =inorden;
		Iterator<V> rta=new Iterator<V>() {
			private Node next=aux.get(posInorden(init));
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return(posInorden((K) next.getKey())<posInorden(end));
			}

			@Override
			public V next() {
				// TODO Auto-generated method stub
				next=aux.get(posInorden((K) next.getKey())+1);
				return (V) next.getValue();
			}
			
		};
		return rta;
		
	}
	
	public Iterator<K> keysInRange(K init, K end){
		ArrayList<Node> aux =inorden;
		Iterator<K> rta=new Iterator<K>() {
			private Node next=aux.get(posInorden(init));
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return(posInorden((K) next.getKey())<posInorden(end));
			}

			@Override
			public K next() {
				// TODO Auto-generated method stub
				next=aux.get(posInorden((K) next.getKey())+1);
				return (K) next.getKey();
			}
			
		};
		return rta;
		
	}
}
