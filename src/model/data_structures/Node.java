package model.data_structures;

public class Node<K,V> {
	
	private final static boolean RED = true;
	private final static boolean BLACK = false;
	private K key;
	private V value;
	int N;
	boolean color;
	private Node<K,V> left;
	private Node<K,V> right;
	
	public Node(K key, V value, int N, boolean color){
		this.setKey(key);
		this.setValue(value);
		this.N = N;
		this.color = color;
	}
	
	private boolean isRed(Node x){
		if(x == null)
			return false;
		return x.color == RED;
	}
	
	private Node<K,V> getLeft(){
		return left;
	}

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}
}
