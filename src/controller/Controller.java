package controller;

import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() throws Exception 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println(modelo.cargarDatos()+"\nM�nimo MOVEMENT_ID "+modelo.datos.min()+"\t M�ximo MOVEMENT_ID "+modelo.datos.max());
				    break;
				    
				case 2:
					System.out.println("Ingrese ID para consultar zona: ");
					int pId = lector.nextInt();
					String a = modelo.consultarZonaPorId(pId);
					System.out.println(a);
					break;
					
				case 3:
					System.out.println("Ingrese ID para consultar zona: \nMin: ");
					int min = lector.nextInt();
					System.out.println("Max: ");
					int max = lector.nextInt();
					String b = modelo.consultarZonaConIdEnRangoZonas(min, max);
					break;

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
