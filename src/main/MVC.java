package main;
import java.io.FileNotFoundException;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import controller.Controller;

public class MVC {
	
	public static void main(String[] args) throws Exception 
	{
		Controller controler = new Controller();
		controler.run();
	}
}
