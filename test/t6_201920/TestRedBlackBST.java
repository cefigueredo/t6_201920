package t6_201920;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.*;

import model.data_structures.RedBlackBST;

public class TestRedBlackBST {

	private RedBlackBST<Object, Object> bst = new RedBlackBST<Object, Object>();
	private final static int TAM = 10;
	@Before
	public void setUp1() throws Exception {
		Random r = new Random();
		for(int i = 0; i < TAM; i++) {
			int n = (int) (r.nextDouble()*TAM);
			bst.put(n, n);
		}
	}
	
	@Test
	public void testContains() throws Exception{
		Random r = new Random();
		int n = (int) (r.nextDouble()*TAM); bst.put(n, n);
		assertTrue(bst.contains(n));

	}
	
	@Test
	public void testGet() throws Exception{
		Random r = new Random();
		int n = (int) (r.nextDouble()*TAM); bst.put(n, n);
		assertEquals(n, bst.get(n));
	}
	
	@Test
	public void testMax() throws Exception{
		int n = 1+TAM; bst.put(n, n);
		assertEquals(n, bst.max());
	}
	
	@Test
	public void testMin() throws Exception{
		int n = 0;
		bst.put(n, n);
		assertEquals(n, bst.min());
	}
	
	@Test
	public void testPut() throws Exception{
		bst = new RedBlackBST<>(); 
		assertTrue(bst.isEmpty());
		bst.put(0, 0);
		bst.put(1, 1);
		assertEquals(0, bst.get(0));
	}
}
